var GP =  {
    gameWidth: 700,
    gameHeight: 400,
    gameFrameTime: 20,
    carRadius: 25,
    frictionMultiplier: 0.97,
    maxSpeed: 6,
    turnSpeed: 0.1,
    acceleration: 0.3
};

function runGameFrame(cars) {
    var impulses = [];
    for(var i = 0; i < cars.length; i++) {
        var car = cars[i];
        // Move the cars
        car.x += car.vx;
        car.y += car.vy;

        // check if cars are close to left and right walls
        if(car.x <= GP.carRadius || car.x >= GP.gameWidth - GP.carRadius) {
            if((car.x <= GP.carRadius && car.vx <= 0) ||
                car.x >= GP.gameWidth - GP.carRadius && car.vx >= 0) {
                // turn the car around
                impulses.push([i, null, 2 * car.vx, 0]);
            }
            if(car.x <= GP.carRadius) car.x = GP.carRadius;
            if(car.x >= GP.gameWidth - GP.carRadius) car.x = GP.gameWidth - GP.carRadius;
        }

        // Top and bottom walls
        if(car.y <= GP.carRadius || car.y >= GP.gameHeight - GP.carRadius) {
            if((car.y <= GP.carRadius && car.vy <= 0) ||
                car.y >= GP.gameHeight - GP.carRadius && car.vy >= 0) {
                // turn the car around
                impulses.push([i, null, 0, 2 * car.vy]);
            }
            if(car.y <= GP.carRadius) car.y = GP.carRadius;
            if(car.y >= GP.gameHeight - GP.carRadius) car.y = GP.gameHeight - GP.carRadius;
        }

        // check car collisions
        for(var j = i+1; j < cars.length; j++) {
            var car2 = cars[j];
            // Distance between two cars
            var distSquared = (car.x - car2.x) * (car.x - car2.x) + (car.y - car2.y) * (car.y - car2.y);

            if(Math.sqrt(distSquared) <= 2 * GP.carRadius) {
                var dx = car2.x - car.x;
                var dy = car2.y - car.y
                var dvx = car.vx - car2.vx;
                var dvy = car.vy - car2.vy;

                var delta = (dx * dvx + dy * dvy) / (dx * dx + dy * dy);
                if(delta <= 0) continue;
                impulses.push([i, j, delta * dx, delta * dy]);
            }
        }
    }

    // apply impulses
    for(var i = 0; i < impulses.length; i++) {
        var impulse = impulses[i];
        if(impulse[0] in cars) {
            cars[impulse[0]].vx -= impulse[2];
            cars[impulse[0]].vy -= impulse[3];
        }
        if(impulse[1] in cars) {
            cars[impulse[1]].vx += impulse[2];
            cars[impulse[1]].vy += impulse[3];
        }
    }

    // enforce speed limit and apply friction
    for(var i = 0; i < cars.length; i++) {
        var car = cars[i];
        var speed = Math.sqrt(car.vx * car.vx + car.vy * car.vy);
        if(speed >= GP.maxSpeed) {
            car.vx *= GP.maxSpeed / speed;
            car.vy *= GP.maxSpeed / speed;
        }

        // friction
        car.vx *= GP.frictionMultiplier;
        car.vy *= GP.frictionMultiplier;
    }
}

if(typeof exports !== "undefined") {
    exports.GP = GP;
    exports.runGameFrame = runGameFrame;
}