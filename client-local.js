var c;
var cars = [];
var myCar = null;
var keysPressed = 0;
var socket = null;
var gameTimer = null;

var carImage = new Image();
carImage.src = "car.png";


var lastKeySentTime = 0;
document.addEventListener("keydown", function(e) {
    var transmit = true;
    if(e.which == 38 && (keysPressed & 1) == 0) keysPressed |= 1;
    else if(e.which == 37 && (keysPressed & 2) == 0) keysPressed |= 2;
    else if(e.which == 39 && (keysPressed & 4) == 0) keysPressed |= 4;
    else transmit = false;

    // Make sure only to send message when socket is ready
    if(transmit && socket && socket.readyState == 1) {
        socket.send(JSON.stringify({
            type: "d",
            data: e.which
        }));
    }
    lastKeySentTime = new Date().getTime();
});

document.addEventListener("keyup", function(e) {
    var transmit = true;
    if(e.which == 38) keysPressed &= ~1;
    else if(e.which == 37) keysPressed &= ~2;
    else if(e.which == 39) keysPressed &= ~4;
    else transmit = false;

    // only send if the key is one of the magic three
    if(transmit && socket && socket.readyState == 1) {
        socket.send(JSON.stringify({
            type: "u",
            data: e.which
        }));
    }
    lastKeySentTime = new Date().getTime();
});

window.addEventListener("load", function() {
    var canvas = document.getElementById("canvas");
    canvas.width = GP.gameWidth;
    canvas.height = GP.gameHeight;
    c = canvas.getContext("2d");

    var name = prompt("What is your username?", "John doe");
    c.textAlign = "center";
    c.fillText("Connection...", GP.gameWidth / 2, GP.gameHeight / 2);

    try {
        if(typeof MozWebSocket !== "undefined") {
            socket = new MozWebSocket("ws://dry-refuge-4933.herokuapp.com");
        } else if(typeof WebSocket !== "undefined") {
            socket = new WebSocket("ws://dry-refuge-4933.herokuapp.com");
        } else {
            socket = null;
            alert("no socket support");
            return false;
        }
    } catch(err) {
        socket = null;
        return false;
    }

    socket.onerror = function(err) {
        //alert("Websocket error: "  + JSON.stringify(err));
    };

    socket.onclose = function(err) {
        //shut down the game loop.
        //if(gameTimer) clearInterval(gameTimer);
        //gameTimer = null;
    };

    socket.onopen = function() {
        // send handshake message
        socket.send(JSON.stringify({
            type: "hi",
            data: name.substring(0, 10)
        }));
        // set up game loop
        gameTimer = setInterval(function() {
                if(myCar) {
                    if(keysPressed & 2) myCar.or -= GP.turnSpeed; // turn left
                    if(keysPressed & 4) myCar.or += GP.turnSpeed; // turn right
                    if(keysPressed & 1) { // accelerate
                        myCar.vx += GP.acceleration * Math.sin(myCar.or);
                        myCar.vy -= GP.acceleration * Math.cos(myCar.or);
                    }
                }
                //runGameFrame(cars);
                drawGame();
            }, GP.gameFrameTime);
    };


    socket.onmessage = function(e) {
        var message;
        // check message format
        try { message = JSON.parse(e.data);} catch(err) {return;}
        if(!("myIndex" in message && "cars" in message)) {
            console.log("Latency:", (new Date().getTime() - lastKeySentTime));
            return;
        }
        // overwrite cars array
        cars = message.cars;
        if(message.myIndex in cars) myCar = cars[message.myIndex];
    }
});

function drawGame() {
    // clear the screen
    c.clearRect(0, 0, GP.gameWidth, GP.gameHeight);

    c.save();
    c.font = "12pt Arial";
    c.fillStyle = "black";
    c.textAlign = "center";

    for(var i = 0; i < cars.length; i++) {
        var car = cars[i];
        c.save();
        c.translate(car.x | 0, car.y | 0);
        c.rotate(car.or);
        c.drawImage(carImage, -carImage.width / 2 | 0, -carImage.height / 2 | 0);
        c.restore();

        if(car.name) {
            c.fillText((car == myCar ? "Me" : car.name.substring(0, 10)), car.x | 0, (car.y - GP.carRadius - 12) | 0);
        }
    }
    c.restore();
}