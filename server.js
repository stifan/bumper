var system = require("util");
var http = require("http");
var webSocketServer = require("websocket").server;
var os  = require('os-utils');

var game = require("./game.js");

var frame = 0;
var framesPerGameStateTransmission = 20;
var maxConnections = 11;
var connections = {};
var lastProcessedInput = [];

var port = Number(process.env.PORT || 3333);

var httpServer = http.createServer(function(req, res) {
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.end();
}).listen(port, function() {
    system.log("Listening for connections on port " + port);
});

// Create websocket server using http server
var server = new webSocketServer({
    httpServer: httpServer,
    closeTimeout: 2000
});

server.on("request", function(req) {
    if(objectSize(connections) >= maxConnections) {
        req.reject();
        return;
    }

    var connection = req.accept(null, req.origin);
    connection.ip = req.remoteAddress;

    // give random id
    do {
        connection.id = Math.floor(Math.random() * 100000);
    } while(connection.id in connections);

    connections[connection.id] = connection;

    connection.on("message", function(message) {
        if(message.type == "utf8") {
            handleClientMessage(connection.id, message.utf8Data);
        }
    });

    connection.on("close", function() {
        handleClientClosure(connection.id);
    });

    system.log("Logged in " + connection.ip + "; currently " + objectSize(connections) + " users.");
});

function objectSize(obj) {
    var size = 0;
    for(var key in obj) {
        if(obj.hasOwnProperty(key)) size++;
    }
    return size;
}

function handleClientClosure(id) {
    if(id in connections) {
        system.log("Disconnect from " + connections[id].ip);
        delete connections[id];
    }
}

function handleClientMessage(id, message) {
    // check that we know this client id and message
    totalBytesReceived += Buffer.byteLength(message);

    if(!(id in connections)) {
        system.log("no id in connections");
        return
    };
    try {
        message = JSON.parse(message);
    } catch (err) {
        system.log("Could not parse message")
        return;
    }
    if(!("type" in message && "data" in message)) {
        system.log("message is wrong format");
        return;
    }

    // handle types of messages
    var connection = connections[id];

    //
    switch(message.type) {
        case "hi":
            if(message.data == "chart") {
                connection.type = "chart";
                system.log(connection.type + " Connected");
                break;
            }
            if(connection.car) break;

            // create player car
            connection.type = "car";
            connection.car = {
                x: game.GP.carRadius + Math.random() * (game.GP.gameWidth - 2 * game.GP.carRadius),
                y: game.GP.carRadius + Math.random() * (game.GP.gameHeight - 2 * game.GP.carRadius),
                vx: 0,
                vy: 0,
                or: 0,
                name: message.data.toString().substring(0, 10)
            };
            // init input bitfield.
            connection.keysPressed = 0;
            system.log(connection.car.name + " spawned a car!");
            sendGameState();
            break;
        case "u": // key up
            if(typeof connection.keysPressed === "undefined") break;
            if(message.data == 37) connection.keysPressed &= ~2; // left
            else if(message.data == 39) connection.keysPressed &= ~4; // right
            else if(message.data == 38) connection.keysPressed &= ~1; // up
            connection.sendUTF(
               JSON.stringify({
                    data: "p"
                })
            );
            break;
        case "d": // key down
            if(typeof connection.keysPressed === "undefined") break;
            system.log(JSON.stringify(message));
            lastProcessedInput[connection.id] = message.id;
            if(message.data == 37) connection.keysPressed |= 2; // left
            else if(message.data == 39) connection.keysPressed |= 4; // right
            else if(message.data == 38) connection.keysPressed |= 1; // up
            connection.sendUTF(
                JSON.stringify({
                    data: "p"
                })
            );
            break;
    }
}

var lastGameStateSend = new Date().getTime();
var totalBytesReceived = 0;
var totalBytesSent = 0;

function sendGameState() {
    var carData = [];
    var indices = {};
    var bytes = 0;
    // collect all car objects
    for(var id in connections) {
        var connection = connections[id];
        if(!connection.car || connection.type == "chart") continue;

        carData.push(connection.car);
        indices[id] = carData.length - 1;
    }

    for(var id in connections) {
        if(connections[id].type == "chart") continue;

        var string = JSON.stringify({
            myIndex: indices[id],
            cars: carData,
            test: "hej",
            lastProcessedInput: lastProcessedInput[connection.id]
        });
        bytes += Buffer.byteLength(string);
        totalBytesSent += bytes;
        connections[id].sendUTF(string);
    }

    var now = new Date().getTime();

    var deltaTime = (now - lastGameStateSend) / 1000 ;
    var memoryUsage = process.memoryUsage();
    var cpuUsage = os.loadavg(1);
    for(var id in connections) {
        if(connections[id].type == "car")  continue;
        connections[id].sendUTF(JSON.stringify({
            bytesSent: bytes,
            totalBytesSent: totalBytesSent,
            bytesReceived: totalBytesReceived,
            deltaTime: deltaTime,
            memoryUsage: memoryUsage,
            platform: os.platform(),
            cpuUsage: cpuUsage
        }));
    }
    lastGameStateSend = now;
    totalBytesReceived = 0;
}

// set up server game loop
setInterval(function() {
    // prepare car data
    var cars = [];
    for(var id in connections) {
        var connection = connections[id];
        if(!connection.car) continue;

        cars.push(connection.car);

        if(connection.keysPressed & 2) connection.car.or -= game.GP.turnSpeed;
        if(connection.keysPressed & 4) connection.car.or += game.GP.turnSpeed;
        if(connection.keysPressed & 1) {
            connection.car.vx += game.GP.acceleration * Math.sin(connection.car.or);
            connection.car.vy -= game.GP.acceleration * Math.cos(connection.car.or);
        }
    }

    game.runGameFrame(cars);

    // inc game frame
    frame = (frame + 1) % framesPerGameStateTransmission;
    if(frame == 0) sendGameState();
}, game.GP.gameFrameTime);