var system = require("util");
var http = require("http");
var webSocketServer = require("websocket").server;

var port = Number(process.env.PORT || 3333);

var frame = 0;
var framesPerTrans = 20;
var maxConnections = 11;
var connections = [];
var lastProcessedInput = [];

var Entity = function() {
    this.x = 0;
    this.speed = 2;
}
Entity.prototype.applyInput = function(input) {
    this.x += input.time * this.speed;
}

var httpServer = http.createServer(function(req, res) {
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.end();
}).listen(port, function() {
    system.log("Listening for connections on port " + port);
});

// Create websocket server using http server
var server = new webSocketServer({
    httpServer: httpServer,
    closeTimeout: 2000
});

server.on("request", function(req) {
    if(objectSize(connections) >= maxConnections) {
        req.reject();
        return;
    }

    var connection = req.accept(null, req.origin);
    connection.ip = req.remoteAddress;

    // give random id
    do {
        connection.id = Math.floor(Math.random() * 100000);
    } while(connection.id in connections);

    connections[connection.id] = connection;

    connection.on("message", function(message) {
        if(message.type == "utf8") {
            handleClientMessage(connection.id, message.utf8Data);
        }
    });

    connection.on("close", function() {
        handleClientClosure(connection.id);
    });

    system.log("Logged in " + connection.ip + "; currently " + objectSize(connections) + " users.");
});

function objectSize(obj) {
    var size = 0;
    for(var key in obj) {
        if(obj.hasOwnProperty(key)) size++;
    }
    return size;
}

function handleClientClosure(id) {
    if(id in connections) {
        system.log("Disconnect from " + connections[id].ip);
        delete connections[id];
    }
}

function handleClientMessage(id, message) {
    // check that we know this client id and message
    if(!(id in connections)) {
        system.log("no id in connections");
        return
    };
    try {
        message = JSON.parse(message);
    } catch (err) {
        system.log("Could not parse message")
        return;
    }
    if(!("type" in message)) {
        system.log("message is wrong format");
        return;
    }

    // handle types of messages
    var connection = connections[id];

    //
    switch(message.type) {
        case "hi":
            system.log("client says hi");
            if(connection.entity) break;

            // create player entity
            connection.entity = new Entity();
            connection.entity.x = 0;
            break;
        case "u": // key up
            system.log("client sends update" + JSON.stringify(message));
            connection.entity.applyInput(message);
            lastProcessedInput[connection.id] = message.seqNum;
            break;
    }
}

function sendWorldState() {
    var data = [];
    var indices = {};

    for(var id in connections) {
        var connection = connections[id];
        if(connection && connection.entity) {
            var string = JSON.stringify({
                id: connection.id,
                x: connection.entity.x,
                lastProcessedInput: lastProcessedInput[connection.id]
            });
            connection.sendUTF(string);
        }
    }
}

function objectSize(obj) {
    var size = 0;
    for(var key in obj) {
        if(obj.hasOwnProperty(key)) size++;
    }
    return size;
}

// set up server game loop
setInterval(function() {
    sendWorldState();
}, 1000/10);
